#ifndef _OUTPUT_DEV_H
#define _OUTPUT_DEV_H

void output_create(int pin);
void output_set(int pin);
void output_clear(int pin);
void output_toggle(int pin);
void output_set_level(int pin, int status);

#endif 

