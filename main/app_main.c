
#include "dev_uart.h"
#include "input_dev.h"
#include "output_dev.h"
#include "dev_uart.h"
#include "esp_log.h"
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"


static const char *TAG = "MAIN";
EventBits_t uxBits;
EventGroupHandle_t xCreatedEventGroup;
#define BUTTON_PRESS_BIT	( 1 << 0 )
#define BIT_RELEASE_BIT	( 1 << 1 )

void uart_task(uint8_t *data, uint16_t length)
{
   if (strstr((char *)data, "ON"))
   {
      ESP_LOGI(TAG, "ON LED");
      output_set_level(2, 1);
   }
   else if(strstr((char *)data, "OFF")){
      ESP_LOGI(TAG, "OFF LED");
      output_set_level(2, 0);      
   }
}
void event_button(int pin)
{
   
   uxBits = xEventGroupSetBits(
                              xCreatedEventGroup,    /* The event group being updated. */
                              BUTTON_PRESS_BIT);/* The bits being set. */
}
void waitbit()
{
   for(;;){
      uxBits = xEventGroupWaitBits(
            xCreatedEventGroup,   /* The event group being tested. */
            BUTTON_PRESS_BIT, /* The bits within the event group to wait for. */
            pdTRUE,        /* BIT_0 & BIT_4 should be cleared before returning. */
            pdFALSE,       /* Don't wait for both bits, either bit will do. */
            100 / portTICK_PERIOD_MS );/* Wait a maximum of 100ms for either bit to be set. */
      if(uxBits & BUTTON_PRESS_BIT){
         uart_put((uint8_t*)"button pressed!\n", 20); 
      } 
   }  
}

void app_main(void)
{  
   uart_set_callback(uart_task);
   uart_init();
   input_create(0, HI_TO_LO);
   input_set_callback(event_button);
   output_create(2);
   xCreatedEventGroup = xEventGroupCreate();
   xTaskCreate(waitbit,"button task press", 2048, NULL, 12, NULL);
}
